# :space_invader: KZT token ERC20
Basic implementation of ERC20 token standart with full-coveraged tests </br> **ETHERSCAN:** https://rinkeby.etherscan.io/address/0x9EAE6C6C5f107D9919B6f82669F2529E28C8EDF2#code
## **Features**</br>
 + Burnable</br>
 + Mintable

## Tasks
- balance (check balance by address)
- transfer (transfer KZT to account by address)
- approve (to allow account to spend KZT of sender)
- transferFrom (to transfer KZT from spender to recipient by sender)
- accounts (standart task to get accounts)
