import "@nomiclabs/hardhat-waffle";
import "@nomiclabs/hardhat-etherscan";
import 'solidity-coverage';

import "./tasks/accounts";
import "./tasks/transfer";
import "./tasks/approve";
import "./tasks/transferFrom";
import "./tasks/balance";

import dotenv from "dotenv";
dotenv.config()

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
  solidity: "0.8.4",
  plugins: ["solidity-coverage"],
  networks: {
    testnet: {
      url: `https://eth-rinkeby.alchemyapi.io/v2/${process.env.API}`,
      accounts: [process.env.PRIVATE_KEY]
    }
  },
  etherscan: {
    apiKey: process.env.ETHERSCAN_API
  },
};
