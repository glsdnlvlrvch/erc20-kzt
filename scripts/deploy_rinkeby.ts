import { ethers } from "hardhat";

import dotenv from "dotenv";
dotenv.config()

async function main() {
    // We get the contract to deploy
    const addr: string = process.env.PUBLIC_KEY;

    const deployer = await ethers.getSigner(addr);
    console.log(
        "Deploying contract with the account:",
        deployer.address
    );

    const Contract = await ethers.getContractFactory("TokenERC20");
    const contract = await Contract.connect(deployer).deploy();

    console.log("Contract deployed to:", contract.address);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });